package config

type Config struct {
	Main        Main        `yaml:"main"`
	Publisher   Publisher   `yaml:"publisher"`
	Mock_Advert Mock_Advert `yaml:"mock_advert"`
}

type Main struct {
	Sum             int    `yaml:"sum"`
	Lead_Cannon     bool   `yaml:"lead_cannon"`
	Redirect_Cannon bool   `yaml:"redirect_cannon"`
	Delay           bool   `yaml:"delay"`
	Time_Delay      int64  `yaml:"time_delay"`
	Lead_status     string `yaml:"lead_status"`
	Scheme          string `yaml:"scheme"`
	Api_Domain      string `yaml:"api_domain"`
}

type Publisher struct {
	Api_Key    string `yaml:"api_key"`
	Hash       string `yaml:"hash"`
	IP         string `yaml:"ip"`
	Utm_camp   string `yaml:"utm_camp"`
	Subaccount string `yaml:"subaccount"`
}

type Mock_Advert struct {
	Api_Key string `yaml:"api_key"`
}
