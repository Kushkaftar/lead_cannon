package urlConstructor

import (
	"net/url"
)

func (uc UrlConfig) GetParameter(uri, param string) (*string, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	q := u.Query()

	reqParam := q.Get(param)
	return &reqParam, nil
}
