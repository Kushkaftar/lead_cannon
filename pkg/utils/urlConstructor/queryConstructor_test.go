package urlConstructor

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	emptyMap   = map[string]string{"": ""}
	oneValue   = map[string]string{"key": "value"}
	twoValue   = map[string]string{"key": "value", "key2": "value2"}
	threeValue = map[string]string{"key": "value", "": "", "key2": "value2"}
)

var queryTable = []struct {
	sheme       string
	domain      string
	path        string
	expected    string
	query       map[string]string
	description string
}{
	{
		sheme:       "http",
		domain:      "example.com",
		path:        "api",
		query:       nil,
		expected:    "http://example.com/api",
		description: "1",
	},
	{
		sheme:       "http",
		domain:      "example.com",
		path:        "",
		query:       emptyMap,
		expected:    "http://example.com",
		description: "2",
	},
	{
		sheme:       "https",
		domain:      "example.com",
		path:        "api/test",
		query:       oneValue,
		expected:    "https://example.com/api/test?key=value",
		description: "3",
	},
	{
		sheme:       "https",
		domain:      "example.com",
		path:        "api/test",
		query:       twoValue,
		expected:    "https://example.com/api/test?key=value&key2=value2",
		description: "4",
	},
	{
		sheme:       "https",
		domain:      "example.com",
		path:        "api/test",
		query:       threeValue,
		expected:    "https://example.com/api/test?key=value&key2=value2",
		description: "5",
	},
}

func TestGeturlQuery(t *testing.T) {
	for _, test := range queryTable {

		urlConfig := NewUrlConfig(test.sheme, test.domain)

		url := urlConfig.GetUrlQuery(test.path, &test.query)

		require.Equal(t, url, test.expected, test.description)
	}

}
