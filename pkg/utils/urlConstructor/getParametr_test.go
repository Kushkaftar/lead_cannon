package urlConstructor

import (
	"github.com/stretchr/testify/require"
	"testing"
)

var getParamTable = []struct {
	url         string
	value       string
	expected    string
	description string
}{
	{
		url:         "https://example.com",
		value:       "",
		expected:    "",
		description: "1",
	},
	{
		url:         "https://example.com/api/",
		value:       "test",
		expected:    "",
		description: "2",
	},
	{
		url:         "https://example.com/api/?test=qwerty",
		value:       "test",
		expected:    "qwerty",
		description: "2",
	},
	{
		url:         "https://example.com/api/?test=qwerty&test=qwerty2",
		value:       "test",
		expected:    "qwerty",
		description: "2",
	},
}

func TestGetParameter(t *testing.T) {
	for _, v := range getParamTable {
		urlConfig := NewUrlConfig("", "")
		query, err := urlConfig.GetParameter(v.url, v.value)
		require.Nil(t, err)

		require.Equal(t, *query, v.expected, v.description)
	}
}
