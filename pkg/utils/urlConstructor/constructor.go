package urlConstructor

import "net/url"

func (uc *UrlConfig) GetUrl(path string) string {
	u := url.URL{
		Scheme: uc.Sheme,
		Host:   uc.Domain,
		Path:   path,
	}

	return u.String()
}
