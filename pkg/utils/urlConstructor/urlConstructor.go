package urlConstructor

type UrlConfig struct {
	Sheme  string
	Domain string
}

func NewUrlConfig(sheme, domain string) *UrlConfig {
	return &UrlConfig{Sheme: sheme, Domain: domain}
}
