package urlConstructor

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var urlTable = []struct {
	sheme       string
	domain      string
	path        string
	expected    string
	description string
}{
	{
		sheme:       "http",
		domain:      "example.com",
		path:        "api",
		expected:    "http://example.com/api",
		description: "1",
	},
	{
		sheme:       "http",
		domain:      "example.com",
		path:        "",
		expected:    "http://example.com",
		description: "2",
	},
	{
		sheme:       "https",
		domain:      "example.com",
		path:        "api/test",
		expected:    "https://example.com/api/test",
		description: "3",
	},
}

func TestGetUrl(t *testing.T) {

	for _, ut := range urlTable {

		urlConfig := NewUrlConfig(ut.sheme, ut.domain)
		url := urlConfig.GetUrl(ut.path)
		require.Equal(t, url, ut.expected, ut.description)
	}
}
