package urlConstructor

import (
	"net/url"
)

func (uc *UrlConfig) GetUrlQuery(path string, values *map[string]string) string {
	u := url.URL{
		Scheme: uc.Sheme,
		Host:   uc.Domain,
		Path:   path,
	}

	q := u.Query()

	if values != nil {

		for key, value := range *values {
			if key != "" {
				q.Add(key, value)
			}

		}
	}

	u.RawQuery = q.Encode()

	return u.String()
}
