package writetofile

import (
	"encoding/json"
	"fmt"
	"lead_cannon/internal/models"
	"os"
)

func WriteToFile(fileName string, leads models.Leads) error {
	path := fmt.Sprintf("output/%s", fileName)
	f, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()
	jsonData, err := json.Marshal(&leads)
	if err != nil {
		return err
	}

	f.WriteString(string(jsonData))

	return nil
}
