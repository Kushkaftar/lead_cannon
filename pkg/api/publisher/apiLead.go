package publisher

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"

	"github.com/bxcodec/faker/v3"
)

const apiLeadPath = "api/lead/create/publisher"

func (p *Publisher) ApiLead() (*client.Response, error) {

	l := newApiLead()
	l.FlowUuid = p.Hash

	l.UtmCampaign = p.UtmCampaign
	//todo: а нужна ли провернка?
	if p.SubAccount != "" {
		l.Subaccount = p.SubAccount
	}

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := p.UrlConstructor.GetUrl(apiLeadPath)

	resp, err := p.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}

	return resp, nil
}

func newApiLead() *ApiLead {
	l := &ApiLead{}
	err := faker.FakeData(&l)
	if err != nil {
		log.Printf("Faker is crushed, err - %s", err)
	}

	l.Ip = "31.131.222.13"
	l.CountryCode = "RU"
	l.Phone = "89100000000"
	return l
}
