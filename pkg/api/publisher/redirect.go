package publisher

import (
	"fmt"
	"lead_cannon/pkg/client"
)

const redirectPath = "/r/"

func (p *Publisher) Redirect() (*client.Response, error) {
	path := fmt.Sprintf("%s%s", redirectPath, p.Hash)
	url := p.UrlConstructor.GetUrl(path)

	resp, err := p.Methods.GetRedirect(url)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
