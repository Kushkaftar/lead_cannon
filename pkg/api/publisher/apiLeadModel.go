package publisher

type ApiLead struct {
	FlowUuid    string `json:"flow_uuid" faker:"-"`
	Ip          string `json:"ip" faker:"-"`
	CountryCode string `json:"country_code" faker:"-"`
	Phone       string `json:"phone" faker:"-"`
	Subaccount  string `json:"subaccount" faker:"-"`
	UtmCampaign string `json:"utm_campaign" faker:"-"`
	UtmContent  string `json:"utm_content" faker:"username"`
	UtmMedium   string `json:"utm_medium" faker:"username"`
	UtmSource   string `json:"utm_source" faker:"username"`
	UtmTerm     string `json:"utm_term" faker:"username"`
	SubId1      string `json:"sub_id1" faker:"username"`
	SubId2      string `json:"sub_id2" faker:"username"`
	SubId3      string `json:"sub_id3" faker:"username"`
	SysSubId1   string `json:"sys_sub_id1" faker:"username"`
	SysSubId2   string `json:"sys_sub_id2" faker:"username"`
	SysSubId3   string `json:"sys_sub_id3" faker:"username"`
	SysSubId4   string `json:"sys_sub_id4" faker:"username"`
	SysSubId5   string `json:"sys_sub_id5" faker:"username"`
	UserAgent   string `json:"user_agent" faker:"-"`
	Referer     string `json:"referer" faker:"domain_name"`
	Name        string `json:"name" faker:"first_name"`
	Comment     string `json:"comment" faker:"-"`
}
