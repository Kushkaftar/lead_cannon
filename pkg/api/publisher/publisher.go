package publisher

import (
	"lead_cannon/pkg/client"
	"lead_cannon/pkg/config"
	"lead_cannon/pkg/utils/urlConstructor"

	"github.com/bxcodec/faker/v3"
)

type Publisher struct {
	Hash           string
	UtmCampaign    string
	SubAccount     string
	IP             string
	Methods        *client.Methods
	UrlConstructor *urlConstructor.UrlConfig
}

func NewPublisher(c config.Config) *Publisher {
	var utmCampaign string
	if c.Publisher.Utm_camp != "" {
		utmCampaign = c.Publisher.Utm_camp
	} else {
		utmCampaign = faker.Name()
	}
	return &Publisher{
		Hash:           c.Publisher.Hash,
		UtmCampaign:    utmCampaign,
		SubAccount:     c.Publisher.Subaccount,
		IP:             c.Publisher.IP,
		Methods:        client.NewMethods(c.Publisher.Api_Key, c.Publisher.IP),
		UrlConstructor: urlConstructor.NewUrlConfig(c.Main.Scheme, c.Main.Api_Domain),
	}
}

type ApiPubLead interface {
	ApiLead() (*client.Response, error)
	GetLeadStatus(leadUuid string) (*client.Response, error)
}

type RedirectPubLead interface {
	Redirect() (*client.Response, error)
	GetUrlToLand(rId string) (*client.Response, error)
	RedirectLead(rid string) (*client.Response, error)
	UpdateVisitToLand(rid, landing_uuid string) (*client.Response, error)
}

type LeadMethod struct {
	ApiPubLead
	RedirectPubLead
}

func NewLeadMethod(c config.Config) *LeadMethod {
	return &LeadMethod{
		ApiPubLead:      NewPublisher(c),
		RedirectPubLead: NewPublisher(c),
	}
}
