package publisher

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const updateVisitToLandPath = "/r/update"

func (p *Publisher) UpdateVisitToLand(rid, landing_uuid string) (*client.Response, error) {
	url := p.UrlConstructor.GetUrl(updateVisitToLandPath)

	l := UpdateVisitToLand{
		RID:         rid,
		LandingUUID: landing_uuid,
	}

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	resp, err := p.Methods.Post(url, jsonData)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
