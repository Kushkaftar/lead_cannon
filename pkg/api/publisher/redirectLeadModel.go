package publisher

type RedirectLead struct {
	RID         string `json:"r_id" faker:"-"`
	CountryCode string `json:"country_code" faker:"-"`
	Phone       string `json:"phone" faker:"-"`
	Name        string `json:"name"  faker:"name"`
	Comment     string `json:"comment"`
	Address     string `json:"address"`
}
