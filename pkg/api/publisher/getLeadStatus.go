package publisher

import "lead_cannon/pkg/client"

const getLeadStatusPath = "/api/lead/get-status"

func (p *Publisher) GetLeadStatus(leadUuid string) (*client.Response, error) {
	query := map[string]string{
		"lead_uuid": leadUuid,
	}
	url := p.UrlConstructor.GetUrlQuery(getLeadStatusPath, &query)

	resp, err := p.Methods.Get(url)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
