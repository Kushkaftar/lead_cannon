package publisher

import (
	//"fmt"
	"lead_cannon/pkg/client"
)

const urlToLandPath = "/r/url"

func (p *Publisher) GetUrlToLand(rId string) (*client.Response, error) {

	query := map[string]string{
		"r_id": rId,
	}
	url := p.UrlConstructor.GetUrlQuery(urlToLandPath, &query)
	resp, err := p.Methods.Get(url)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
