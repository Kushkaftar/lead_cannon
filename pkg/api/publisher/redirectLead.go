package publisher

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"

	"github.com/bxcodec/faker/v3"
)

const redirectLeadPath = "/l/create"

func (p *Publisher) RedirectLead(rid string) (*client.Response, error) {
	l := newRedirectLead()
	l.RID = rid

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := p.UrlConstructor.GetUrl(redirectLeadPath)

	resp, err := p.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}

	return resp, nil
}

func newRedirectLead() *RedirectLead {
	l := &RedirectLead{}
	err := faker.FakeData(&l)
	if err != nil {
		log.Printf("Faker is crushed, err - %s", err)
	}

	l.CountryCode = "RU"
	l.Phone = "89100000000"

	return l
}
