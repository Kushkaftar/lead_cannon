package publisher

type UpdateVisitToLand struct {
	RID         string `json:"r_id"`
	LandingUUID string `json:"landing_uuid"`
}
