package advertiser

import (
	"lead_cannon/pkg/client"
	"lead_cannon/pkg/config"
	"lead_cannon/pkg/utils/urlConstructor"
)

type Advertiser struct {
	Methods        client.Methods
	UrlConstructor urlConstructor.UrlConfig
}

func NewAdvertiser(c config.Config) *Advertiser {
	return &Advertiser{
		Methods:        *client.NewMethods(c.Mock_Advert.Api_Key, c.Publisher.IP),
		UrlConstructor: *urlConstructor.NewUrlConfig(c.Main.Scheme, c.Main.Api_Domain),
	}
}

type AdvertiserMethod interface {
	LeadApproved(leadUuid string) (*client.Response, error)
	LeadTrash(leadUuid string, trashReason int) (*client.Response, error)
	LeadReject(leadUuid string) (*client.Response, error)
	ChangeOrderID(leadUuid string) (*client.Response, error)
	LeadData(orderID string, laedQuality, age int) (*client.Response, error)
	LeadMoney(leadUuid string, publisherRevenue, advertiserRevenue string) (*client.Response, error)
}

type AdvertiserMethods struct {
	AdvertiserMethod
}

func NewAdvertiserMethods(c config.Config) *AdvertiserMethods {
	return &AdvertiserMethods{
		AdvertiserMethod: NewAdvertiser(c),
	}
}
