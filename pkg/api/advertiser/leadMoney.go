package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const updateLeadMoneyPath = "/api/lead/update-money"

func (a *Advertiser) LeadMoney(leadUuid string, publisherRevenue, advertiserRevenue string) (*client.Response, error) {
	leadData := newLeadMoney(leadUuid, publisherRevenue, advertiserRevenue)
	// log.Printf("LeadMoney - %+v", leadData)

	jsonData, err := json.Marshal(&leadData)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(updateLeadMoneyPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	// log.Printf("LeadMoney: ордер ид - %s, время запроса time %s", leadData.OrderID, resp.ResponseTime)
	// log.Printf("LeadMoney: orderID - %s, response - %s", leadData.OrderID, string(resp.Body))
	return resp, nil
}

func newLeadMoney(leadUuid string, publisherRevenue, advertiserRevenue string) *LeadMoney {

	return &LeadMoney{
		OrderID:           leadUuid,
		PublisherRevenue:  publisherRevenue,
		AdvertiserRevenue: advertiserRevenue,
		IsCustomRevenue:   true,
	}
}
