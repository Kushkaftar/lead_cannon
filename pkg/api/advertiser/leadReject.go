package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const rejectPath = "/api/lead/reject"

func (a *Advertiser) LeadReject(leadUuid string) (*client.Response, error) {
	//TODO: все методы практически одинаковые может конструктор?

	l := newReject(leadUuid)

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(rejectPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	return resp, nil
}

func newReject(leadUuid string) *Reject {
	return &Reject{
		OrderID:      leadUuid,
		RejectReason: 606,
		Status:       true,
	}
}
