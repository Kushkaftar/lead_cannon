package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const trashPath = "/api/lead/trash"

func (a *Advertiser) LeadTrash(leadUuid string, trashReason int) (*client.Response, error) {
	//TODO: все методы практически одинаковые может конструктор?

	l := newTrash(leadUuid, trashReason)

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(trashPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	return resp, nil
}

func newTrash(leadUuid string, trashReason int) *Trash {
	return &Trash{
		OrderID:     leadUuid,
		TrashReason: trashReason,
		IsFraud:     true,
	}
}
