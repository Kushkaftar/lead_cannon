package advertiser

type Approved struct {
	OrderID string `json:"order_id"`
}

type Reject struct {
	OrderID      string `json:"order_id"`
	RejectReason int    `json:"reject_reason"`
	Status       bool   `json:"status"`
}

type Trash struct {
	OrderID     string `json:"order_id"`
	TrashReason int    `json:"trash_reason"`
	IsFraud     bool   `json:"is_fraud"`
}

type ChangeOrder struct {
	OrderID    string `json:"order_id"`
	NewOrderID string `json:"new_order_id"`
}

type LeadData struct {
	OrderId       string `json:"order_id"`
	Age           int    `json:"age"`
	Gender        string `json:"gender,omitempty"`
	Price         string `json:"price"`
	PriceCurrency string `json:"price_currency,omitempty"`
	CallsCount    int    `json:"calls_count,omitempty"`
	OrderQuality  int    `json:"order_quality"`
	CallAnswered  bool   `json:"call_answered,omitempty"`
}

type LeadMoney struct {
	OrderID           string `json:"order_id"`
	OrderAmount       string `json:"order_amount,omitempty"`
	AdvertiserRevenue string `json:"advertiser_revenue"`
	PublisherRevenue  string `json:"publisher_revenue"`
	IsCustomRevenue   bool   `json:"is_custom_revenue"`
}
