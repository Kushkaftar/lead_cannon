package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const updateLeadDataPath = "/api/lead/update-order-data"

func (a *Advertiser) LeadData(orderID string, laedQuality, age int) (*client.Response, error) {
	leadMoney := newLeadData(orderID, laedQuality, age)

	jsonData, err := json.Marshal(&leadMoney)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(updateLeadDataPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	return resp, nil
}

func newLeadData(orderID string, laedQuality, age int) *LeadData {
	return &LeadData{
		OrderId:      orderID,
		OrderQuality: laedQuality,
		Price:        "1111",
		Age:          age,
	}
}
