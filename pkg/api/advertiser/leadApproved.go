package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const approvedPath = "/api/lead/approve"

func (a *Advertiser) LeadApproved(leadUuid string) (*client.Response, error) {
	//TODO: все методы практически одинаковые может конструктор?

	l := newApproved(leadUuid)

	jsonData, err := json.Marshal(&l)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(approvedPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	return resp, nil
}

func newApproved(leadUuid string) *Approved {
	return &Approved{
		OrderID: leadUuid,
	}
}
