package advertiser

import (
	"encoding/json"
	"lead_cannon/pkg/client"
	"log"
)

const changeOrderIDPath = "/api/lead/update-order-id"

func (a *Advertiser) ChangeOrderID(leadUuid string) (*client.Response, error) {
	newOrderID := newOrderID(leadUuid)

	jsonData, err := json.Marshal(&newOrderID)
	if err != nil {
		log.Printf("Json marshal crushed, err - %s", err)
		return nil, err
	}

	url := a.UrlConstructor.GetUrl(changeOrderIDPath)

	resp, err := a.Methods.Post(url, jsonData)
	if err != nil {
		log.Printf("err - %s", err)
		return nil, err
	}
	// log.Printf("ChangeOrderID: ордер ид - %s, время запроса time %s", newOrderID.NewOrderID, resp.ResponseTime)
	// log.Printf("ChangeOrderID response - %+v", string(resp.Body))
	return resp, nil
}

func newOrderID(leadUuid string) *ChangeOrder {
	return &ChangeOrder{
		OrderID:    leadUuid,
		NewOrderID: leadUuid + "-LC",
	}
}
