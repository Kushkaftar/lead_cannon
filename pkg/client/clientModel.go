package client

import (
	"net/http"
	"time"
)

type Response struct {
	Header       http.Header
	Body         []uint8
	StatusCode   int
	ResponseTime time.Time
}

type ClientConfig struct {
	URL     string
	Method  string
	Headers []Headers
	Body    []uint8
}

type Headers struct {
	HeaderKey   string
	HeaderValue string
}
