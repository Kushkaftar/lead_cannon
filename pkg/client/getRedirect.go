package client

import (
	"go.uber.org/zap"
)

func (c *Client) GetRedirect(url string) (*Response, error) {

	cc := ClientConfig{
		URL:    url,
		Method: "GET",
		Body:   nil,
		Headers: []Headers{
			{
				HeaderKey:   "X-Forwarded-For",
				HeaderValue: c.IP,
			},
		},
	}
	resp, err := cc.request()
	if err != nil {
		c.logg.Error("method GetRedirect ERROR",
			zap.Error(err),
			zap.String("url", url))
		return nil, err
	}

	if resp.StatusCode != 302 {
		c.logg.Info("method GetRedirect",
			zap.String("url", url),
			zap.Any("response headers", resp.Header),
			zap.Int("response code", resp.StatusCode))
	}

	return resp, nil
}
