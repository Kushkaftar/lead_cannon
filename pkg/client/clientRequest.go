package client

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"time"
)

func (c ClientConfig) request() (*Response, error) {
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}

	req, err := http.NewRequest(
		c.Method, c.URL, bytes.NewBuffer(c.Body),
	)
	if err != nil {
		return nil, err
	}

	if len(c.Headers) != 0 {

		for _, v := range c.Headers {
			req.Header.Set(v.HeaderKey, v.HeaderValue)
		}
	}
	response, err := client.Do(req)
	now := time.Now()
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	resp := Response{}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	resp.ResponseTime = now
	resp.Body = body
	resp.Header = response.Header
	resp.StatusCode = response.StatusCode

	return &resp, nil
}
