package client

import (
	"go.uber.org/zap"
	"lead_cannon/pkg/logger"
)

type Client struct {
	ApiKey string
	IP     string
	logg   *zap.Logger
}

func NewClient(apiKey string, ip string) *Client {
	return &Client{ApiKey: apiKey, IP: ip, logg: logger.NewLogger()}
}

type Method interface {
	Get(url string) (*Response, error)
	GetRedirect(url string) (*Response, error)
	Post(url string, body []uint8) (*Response, error)
}

type Methods struct {
	Method
}

func NewMethods(apiKey string, ip string) *Methods {
	return &Methods{Method: NewClient(apiKey, ip)}
}
