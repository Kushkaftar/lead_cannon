package client

import (
	"fmt"
	"go.uber.org/zap"
)

func (c *Client) Post(url string, body []uint8) (*Response, error) {

	cc := ClientConfig{
		URL:    url,
		Method: "POST",
		Body:   body,
		Headers: []Headers{
			{
				HeaderKey:   "Content-Type",
				HeaderValue: "application/json",
			},
			{
				HeaderKey:   "ApiKey",
				HeaderValue: c.ApiKey,
			},
		},
	}

	resp, err := cc.request()
	if err != nil {
		c.logg.Error("method POST ERROR",
			zap.Error(err),
			zap.String("url", url),
			zap.String("body", fmt.Sprintf("%s", string(body))))
		return nil, err
	}

	if resp.StatusCode != 200 {
		c.logg.Warn("method POST",
			zap.String("url", url),
			zap.String("body", fmt.Sprintf("%s", string(body))),
			zap.Any("response headers", resp.Header),
			zap.Int("response code", resp.StatusCode))
	}

	return resp, nil
}
