package client

import "go.uber.org/zap"

func (c *Client) Get(url string) (*Response, error) {

	cc := ClientConfig{
		URL:    url,
		Method: "GET",
		Body:   nil,
		Headers: []Headers{
			{
				HeaderKey:   "ApiKey",
				HeaderValue: c.ApiKey,
			},
		},
	}
	resp, err := cc.request()
	if err != nil {
		c.logg.Error("method Get ERROR",
			zap.Error(err),
			zap.String("url", url))

		return nil, err
	}

	if resp.StatusCode != 200 {
		c.logg.Info("method Get",
			zap.String("url", url),
			zap.Any("response headers", resp.Header),
			zap.Int("response code", resp.StatusCode))
	}

	return resp, nil
}
