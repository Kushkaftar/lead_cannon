package main

import (
	"flag"
	"lead_cannon/internal/service"
	"log"

	"lead_cannon/pkg/config"
)

const pathToConfig = "./configs"

func main() {
	var fileName string

	flag.StringVar(&fileName, "env", "", "use flag \"-env\" for config file name")
	flag.Parse()

	c, err := config.NewConfig(fileName, pathToConfig)
	if err != nil {
		log.Fatal("application exited with an error")
	}

	service.Run(c)
}
