package service

import (
	"lead_cannon/internal/leadCannon"
	"lead_cannon/internal/redirectCannon"
	"lead_cannon/pkg/config"
	"sync"
)

func Run(c *config.Config) {
	var wg sync.WaitGroup

	if c.Main.Lead_Cannon {
		wg.Add(1)

		go func(c *config.Config) {
			leadCannon.Run(c)
			wg.Done()
		}(c)

	}

	if c.Main.Redirect_Cannon {
		wg.Add(1)

		go func(c *config.Config) {
			redirectCannon.Run(c)
			wg.Done()
		}(c)
	}

	wg.Wait()
}
