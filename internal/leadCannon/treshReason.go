package leadCannon

import (
	"math/rand"
	"time"
)

var trashReason [40]int = [40]int{
	301,
	302,
	303,
	304,
	305,
	306,
	307,
	308,
	309,
	310,
	311,
	312,
	401,
	402,
	403,
	404,
	405,
	406,
	407,
	408,
	409,
	410,
	411,
	412,
	413,
	414,
	415,
	416,
	417,
	418,
	419,
	420,
	421,
	422,
	423,
	424,
	425,
	426,
	427,
	428}

func newTrashReason() int {
	rand.Seed(time.Now().UnixNano())
	return trashReason[rand.Intn(len(trashReason))]
}
