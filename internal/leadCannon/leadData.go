package leadCannon

import (
	"encoding/json"
	"lead_cannon/internal/models"
)

func leadDatas(leadData models.Lead) string {

	jsonData, err := json.Marshal(&leadData)
	if err != nil {
		return err.Error()
	}
	return string(jsonData)
}
