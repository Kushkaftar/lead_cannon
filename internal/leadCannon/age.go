package leadCannon

import (
	"math/rand"
	"time"
)

func newAge() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(40) + 20
}
