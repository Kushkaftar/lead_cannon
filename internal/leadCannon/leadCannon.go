package leadCannon

import (
	"fmt"
	"lead_cannon/internal/models"
	"lead_cannon/pkg/api/advertiser"
	"lead_cannon/pkg/api/publisher"
	"lead_cannon/pkg/config"
	writetofile "lead_cannon/pkg/utils/writeToFile"
	"log"
	"time"
)

type LeadCannon struct {
	pub *publisher.LeadMethod
	adv *advertiser.AdvertiserMethods
}

func NewLeadCannon(c *config.Config) *LeadCannon {
	return &LeadCannon{
		pub: publisher.NewLeadMethod(*c),
		adv: advertiser.NewAdvertiserMethods(*c),
	}
}

//todo найти ошибку: если хеш не валидный, не все записи с ошибкой

func Run(c *config.Config) {
	s := NewLeadCannon(c)

	ch := make(chan models.Lead)

	if c.Main.Delay {
		duration := time.Duration(c.Main.Time_Delay) * time.Millisecond
		tic := time.NewTicker(duration)

		i := 1
		for range tic.C {
			i++
			go sendLead(c.Main.Lead_status, s, ch)

			if i > c.Main.Sum {
				tic.Stop()
				break
			}
		}
	} else {
		for i := 0; i < c.Main.Sum; i++ {

			go sendLead(c.Main.Lead_status, s, ch)

		}
	}

	now := time.Now().Unix()
	fileName := fmt.Sprintf("%dlead.json", now)
	log.Println(fileName)

	var leads models.Leads

	for i := 0; i < c.Main.Sum; i++ {
		leads.Leads = append(leads.Leads, <-ch)

	}

	err := writetofile.WriteToFile(fileName, leads)
	if err != nil {
		log.Println(err)
	}
}
