package leadCannon

import (
	"encoding/json"
	"fmt"
	"lead_cannon/pkg/client"
)

func getOrderID(cl *client.Response) (*string, error) {

	if cl.StatusCode != 200 {
		err := fmt.Errorf("status code is %d, body - %s", cl.StatusCode, string(cl.Body))
		return nil, err
	}

	body := LeadApiResp{}
	err := json.Unmarshal(cl.Body, &body)
	if err != nil {
		return nil, err
	}

	if !body.Success {
		err := fmt.Errorf("success is not true, body - %+v", string(cl.Body))
		return nil, err
	}
	return &body.Data.OrderID, nil
}
