package leadCannon

type LeadApiResp struct {
	Success bool `json:"success"`
	Data    Data `json:"data"`
}

type Data struct {
	LeadUUID string `json:"lead_uuid"`
	OrderID  string `json:"order_id,omitempty"`
}
