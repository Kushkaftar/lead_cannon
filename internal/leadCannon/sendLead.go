package leadCannon

import (
	"lead_cannon/internal/models"
	"log"
	"math/rand"
	"time"
)

const (
	_ = iota
	normal_lead
	last_call
	black_list
)

const (
	_ = iota
	new
	approved
	reject
	trash
)

func sendLead(configStatus string, s *LeadCannon, ch chan models.Lead) {
	var leadData models.Lead

	l, err := s.pub.ApiLead()
	if err != nil {
		leadData.Errors = err.Error()
		ch <- leadData
		return
	}

	// TODO del responseTime
	//responseTime := l.ResponseTime

	leadUUID, err := CheckRespError(l)
	if err != nil {
		leadData.Errors = err.Error()
		ch <- leadData
		return
	}

	// add lead uuid to leadData struct
	leadData.UUID = *leadUUID
	leadData.OrderID = *leadUUID
	leadData.LeadQuality = normal_lead
	leadData.Created = l.ResponseTime

	var status string

	if configStatus == "random" {
		option := []string{"trash", "reject", "new", "approved", "last_call", "black_list"}

		rand.Seed(time.Now().UnixNano())
		status = option[rand.Intn(len(option))]

	} else {
		status = configStatus
	}

	switch status {
	case "new":
		// add lead status and quality to leadData struct
		leadData.Status = new

		//str := fmt.Sprintf("lead status - %s, uuid - %s, created - %s", status, *leadUUID, responseTime)
		ch <- leadData

	case "approved":
		al, err := s.adv.LeadApproved(*leadUUID)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(al)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// add lead status and quality to leadData struct
		leadData.Status = approved

		//str := fmt.Sprintf("lead status - %s, lead quality - normal_lead, uuid - %s, created - %s", status, *uuid, responseTime)
		ch <- leadData

	case "reject":
		al, err := s.adv.LeadReject(*leadUUID)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(al)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		leadData.Status = reject

		ch <- leadData

	case "trash":
		var age int

		trashReason := newTrashReason()

		if trashReason == 403 {
			age = 17
		} else {
			age = newAge()
		}

		// апдейт данных лида
		updateData, err := s.adv.LeadData(*leadUUID, normal_lead, age)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(updateData)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		al, err := s.adv.LeadTrash(*leadUUID, trashReason)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}
		_, err = CheckRespError(al)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// add lead status and trash reason to leadData struct
		leadData.Status = trash
		leadData.TrashReason = trashReason

		ch <- leadData

	case "last_call":
		// реджект лида
		al, err := s.adv.LeadReject(*leadUUID)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		uuid, err := CheckRespError(al)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// смена ордер id
		newOrder, err := s.adv.ChangeOrderID(*uuid)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		orderID, err := getOrderID(newOrder)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// апдейт данных лида, проставление признака второго звонка
		updateData, err := s.adv.LeadData(*orderID, last_call, newAge())
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(updateData)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// апрув лида
		appriveLastCall, err := s.adv.LeadApproved(*orderID)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(appriveLastCall)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// update lead data
		leadData.OrderID = *orderID
		leadData.Status = approved
		leadData.LeadQuality = last_call

		ch <- leadData

	case "black_list":

		// апдейт данных лида, проставление признака черного списка
		updateData, err := s.adv.LeadData(*leadUUID, black_list, newAge())
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		_, err = CheckRespError(updateData)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// апрув лида
		appriveLastCall, err := s.adv.LeadApproved(*leadUUID)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}
		// log.Printf("===approve LC - %s", string(appriveLastCall.Body))

		_, err = CheckRespError(appriveLastCall)
		if err != nil {
			log.Println(err)
			leadData.Errors = err.Error()
			ch <- leadData
			return
		}

		// update lead data
		leadData.OrderID = *leadUUID
		leadData.Status = approved
		leadData.LeadQuality = black_list

		//str := fmt.Sprintf("lead status - approved, lead quality - %s, uuid - %s, created - %s", status, *uuid, responseTime)
		ch <- leadData

	default:
		leadData.Errors = "value lead_status cannot be processed"
		ch <- leadData
		log.Println("value lead_status cannot be processed")
		return
	}

}
