package redirectCannon

import (
	"encoding/json"
	"fmt"
)

const (
	rIdParam         = "r_id"
	landingUuidParam = "landing_uuid"
)

func createRedirect(r *RedirectCannon, ch chan string) {

	rid, err := r.getRID()
	if err != nil {
		ch <- err.Error()
	}

	str := fmt.Sprintf("r_id - %s", rid)

	landUuid, err := r.getLandingUuid(rid)
	if err != nil {
		ch <- err.Error()
	}

	//log.Printf("-- landUuid - %s", landUuid)
	str = fmt.Sprintf("%s, landing_uuid - %s", str, landUuid)

	success, err := r.updateVisitToLand(rid, landUuid)
	if err != nil {
		ch <- err.Error()
	}
	str = fmt.Sprintf("%s, update redirect - %t", str, success)
	ch <- str
}

func (r *RedirectCannon) getRID() (string, error) {
	resp, err := r.pub.Redirect()
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 302 {
		err := fmt.Errorf("status code - %d", resp.StatusCode)
		return "", err
	}

	// TODO: do refactoring
	lc := resp.Header["Location"][0]

	rid, err := r.constructor.GetParameter(lc, rIdParam)
	if err != nil {
		return "", err
	}

	return *rid, nil
}

func (r *RedirectCannon) getLandingUuid(rid string) (string, error) {
	resp, err := r.pub.GetUrlToLand(rid)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		err := fmt.Errorf("status code - %d", resp.StatusCode)
		return "", err
	}

	urlToLand := GetUrlToLand{}
	err = json.Unmarshal(resp.Body, &urlToLand)
	if err != nil {
		return "", err
	}

	if !urlToLand.Success {
		err := fmt.Errorf("success - %t", urlToLand.Success)
		return "", err
	}

	landUuid, err := r.constructor.GetParameter(urlToLand.Data.URL, landingUuidParam)
	if err != nil {
		return "", err
	}

	return *landUuid, nil
}

func (r *RedirectCannon) updateVisitToLand(rid, landingUuid string) (bool, error) {
	resp, err := r.pub.UpdateVisitToLand(rid, landingUuid)
	if err != nil {
		return false, err
	}

	if resp.StatusCode != 200 {
		err := fmt.Errorf("status code - %d", resp.StatusCode)
		return false, err
	}
	var success struct {
		Success bool `json:"success"`
	}
	err = json.Unmarshal(resp.Body, &success)
	if err != nil {
		return false, err
	}

	if !success.Success {
		err := fmt.Errorf("success - %t", success.Success)
		return false, err
	}

	return true, nil
}
