package redirectCannon

import (
	"fmt"
	"lead_cannon/pkg/api/publisher"
	"lead_cannon/pkg/config"
	"lead_cannon/pkg/utils/urlConstructor"
	"log"
	"time"
)

type RedirectCannon struct {
	pub         *publisher.LeadMethod
	constructor urlConstructor.UrlConfig
}

func NewRedirectCannon(c *config.Config) *RedirectCannon {
	return &RedirectCannon{
		pub:         publisher.NewLeadMethod(*c),
		constructor: *urlConstructor.NewUrlConfig(c.Main.Scheme, c.Main.Api_Domain),
	}
}

func Run(c *config.Config) {
	r := NewRedirectCannon(c)
	ch := make(chan string)

	if c.Main.Delay {
		duration := time.Duration(c.Main.Time_Delay) * time.Millisecond
		tic := time.NewTicker(duration)
		i := 1
		for range tic.C {
			i++
			go createRedirect(r, ch)

			if i > c.Main.Sum {
				tic.Stop()
				break
			}
		}
	} else {
		for i := 0; i < c.Main.Sum; i++ {
			go createRedirect(r, ch)
		}

	}

	now := time.Now().Unix()
	fileName := fmt.Sprintf("%dRedirect.txt", now)
	log.Println(fileName)

	for i := 0; i < c.Main.Sum; i++ {

		// TODO: !!! refactor to struct !!!

		// err := writetofile.WriteToFile(fileName, <-ch)
		// if err != nil {
		// 	log.Println(err)
		// }
	}

}
