package models

import "time"

type Leads struct {
	Leads []Lead `json:"leads"`
}

type Lead struct {
	UUID        string    `json:"uuid"`
	OrderID     string    `json:"order_id"`
	Status      int       `json:"status"`
	LeadQuality int       `json:"lead_quality,omitempty"`
	TrashReason int       `json:"trash_reason,omitempty"`
	Created     time.Time `json:"created"`
	Errors      string    `json:"errors,omitempty"`
}
